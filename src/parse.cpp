#include <algorithm>
#include <fstream>
#include <iterator>
#include <regex>
#include <string>
#include <vector>
#include <iostream>

#include "parse.h"

std::vector<container_type_t> parse1(const std::string &file) {

  // Result container
  std::vector<container_type_t> out;

  // Open input file
  std::ifstream in{file};

  // Tokenise each line using a comma delimiter
  size_t lines = 0;

  std::string line;
  while (std::getline(in, line)) {

	  ++lines;

    std::regex reg("\\s*,\\s*");
    std::sregex_token_iterator iter(line.begin(), line.end(), reg, -1);
    std::sregex_token_iterator end;

    std::vector<std::string> vec(iter, end);

    // Convert to expected type
    std::transform(vec.cbegin(), vec.cend(), std::back_inserter(out),
                   [](const std::string &s) { return s; /* std::stod(s); */ });
  }

  std::cout << lines << " lines\n";

  return out;
}
