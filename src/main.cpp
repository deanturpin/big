#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>

#include "parse.h"

int main(int argc, char** argv) {

  std::stringstream params;
  for (int i = 1; i < argc; ++i)
	  params << argv[i];

  // Use supplied param or default
  const std::string file{
	  params.str().empty() ? "tmp/big.csv" : params.str()};

  std::cout << "Reading " << file << "\n";

  // Parse CSV
  const auto &big = parse1(file);

  // Print summary
  std::for_each_n(big.cbegin(), std::min(big.size(), 10ul),
                  [](const auto &i) { std::cout << i << "\t"; });

  std::cout << "\n" << big.size() << " data points\n";
}
