# Compilers
CXX := g++-10

# Cut down flags for projects that are expected to have warnings
CXXFLAGS_CORE := --std=c++20 --all-warnings --extra-warnings

# gcc flags
GCCCXXFLAGS := $(CXXFLAGS_CORE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wattribute-alias -Wformat-overflow -Wformat-truncation \
	-Wmissing-attributes -Wstringop-truncation -Wdeprecated-copy \
	-Wclass-conversion -Og -g

CLANGCXXFLAGS := $(CXXFLAGS_CORE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy -Wc++98-c++11-c++14-c++17-compat-pedantic

CXXFLAGS := $(GCCCXXFLAGS)

all: tmp tmp/big lint run

# All objects go in tmp for easy cleaning
tmp:
	mkdir -p $@

# Make object from a cpp
tmp/%.o: src/%.cpp tmp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

OBJECTS := tmp/main.o tmp/parse.o
CSV := tmp/big.csv

# Make the main application
tmp/big: $(OBJECTS)
	# $(MAKE) -j $(shell nproc) $(OBJECTS) $(CSV)
	$(CXX) -o $@ $(CXXFLAGS) $^

run: tmp/big $(CSV)
	./$< $(CSV)
	head -10 $(CSV)

# Helper targets

$(CSV): tmp
	bin/generate-csv.sh > $@

format:
	clang-format -i src/*.h
	clang-format -i src/*.cpp

lint:
	@sloccount . | grep -E 'Total Estimated Cost to Develop|Total Physical Source Lines of Code'
	@cppcheck --enable=all src

apt:
	apt update
	apt install --yes g++-10 make clang-format sloccount cppcheck iwyu

iwyu:
	$(shell make tmp/big CXX=iwyu CXXFLAGS=--std=c++2a)

# Everything is compiled/generated in tmp
clean:
	rm -rf tmp

