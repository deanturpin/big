# Project features
- [ ] Big data
- [ ] Opportunities for parallelism

## Language features (including C++20)
- [ ] coroutines
- [ ] spans
- [ ] fmt lib
- [x] regex

## Build / analyse / test
- [x] Latest Ubuntu (groovy)
- [x] sloccount
- [x] iwyu
- [x] cppcheck
- [x] Build in parallel (make)
- [ ] Tracy profiling 
- [ ] Google mocks testing
- [ ] Code coverage
- [ ] clang format on commit

# Parser functionality
- [x] Means to ingest data: CSV parser
- [x] Script to generate test data
- [ ] Detect heading row
- [ ] Detect type (data, double)
- [ ] Use std::chrono for data

# Install dependencies
```bash
make apt
```

# Build and run
```bash
make
```

# References
- [Inspirational big data sources](https://turpin.dev/post/big/)
- [Side hustle ideas](https://turpin.dev/post/hustle/)
- [FMT lib](https://github.com/fmtlib/fmt)

